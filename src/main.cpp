// ESP32 Encoder
/* This device operates the Analog Photo-Encoder (APE)
The ESP32 uses SPI to communicate between other devices
to send and receive encoder values as settings. This device
will act as the slave to the SPI unit. To communicate with
the encoder, a basic custom communication protocol is setup
which is as follows:
    Convert int to binary:
    0 = Standby
    1 = Query
    100 = start calibration mode
    110 = step motor done
    102 = ready read encoder
    120 = read encoder done
    103 = finish calibration

Using this protocol, the procedure to communicate the entire calibration
process is as follows:
    1. Master: ping slave (optional to check if slave exists)
        Slave: ack (If slave does not ack, then fail calibration (try again))
    2. Master: start calibration mode (change encoder to calibration mode)
        Slave: ack
    3. Master: send calibration info (optional parameters to be sent)
        Slave: ack
        * repeat as necessary *
    4. Master: step motor done (move motor to reset backlash and send done)
        Slave: read encoder done (reads the encoder values then send done)
        Master: ready motor (status update check on slave being done)
        Slave: ready read encoder (status update if not done)
    5. Master: step motor done (steps motor after reading "read encoder done" then send)
        Slave: read encoder done (reads the encoder values then send)
        Master: ready motor (status update check on slave being done)
        Slave: ready read encoder (status update if not done)
        * Repeat step 5 *
    6. Master: finish calibration (tells slave that calibration is wrapping up)
        Slave: ack (returns standby until slave is done, then ack)
        * Done *
 */
 
#include <Arduino.h>
#include <ESP32SPISlave.h>

using namespace std;

// Encoder Pins
#define ENC_X       36
#define ENC_Y       39
#define ENC_C       34
#define ENC_S       35

// SPI protocol
#define STANDBY     0
#define QUERY       1
#define START_CAL   100
#define DONE_STEP   110
#define READING_ENC 102
#define DONE_ENC    120
#define DONE_CAL    103

ESP32SPISlave slave;
static constexpr uint32_t BUFFER_SIZE {2};
uint8_t spi_slave_tx_buf[BUFFER_SIZE];
uint8_t spi_slave_rx_buf[BUFFER_SIZE];

volatile uint16_t EncoderCommand, EncoderRespond;
uint16_t PreviousCommand = 0;

volatile bool newCommand = false;
volatile bool calibrationMode = false;
volatile bool stillReading = false;
volatile bool startReading = false;

volatile int commandSignal = 0;

float encX, encY, encC, encS;
uint64_t step_count = 0;

constexpr uint8_t CORE_TASK_SPI_SLAVE {0};
constexpr uint8_t CORE_TASK_PROCESS_BUFFER {0};

static TaskHandle_t task_handle_wait_spi = 0;
static TaskHandle_t task_handle_process_buffer = 0;

void task_wait_spi(void* pvParameters) {
    while (1) {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        // block until the transaction comes from master
        slave.wait(spi_slave_rx_buf, spi_slave_tx_buf, BUFFER_SIZE);

        xTaskNotifyGive(task_handle_process_buffer);
    }
}

void task_process_buffer(void* pvParameters) {
    while (1) {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        // do something with `spi_slave_rx_buf`
        EncoderCommand = 256U*spi_slave_rx_buf[0]+spi_slave_rx_buf[1];
        EncoderRespond = 256U*spi_slave_tx_buf[0]+spi_slave_tx_buf[1];
        newCommand = true;
        slave.pop();

        xTaskNotifyGive(task_handle_wait_spi);
    }
}


void setup() {
    Serial.begin(250000);
    pinMode(ENC_X, INPUT);
    pinMode(ENC_Y, INPUT);
    pinMode(ENC_C, INPUT);
    pinMode(ENC_S, INPUT);

    slave.setDataMode(SPI_MODE0);
    slave.begin(VSPI);

    xTaskCreatePinnedToCore(task_wait_spi, "task_wait_spi", 2048, NULL, 2, &task_handle_wait_spi, CORE_TASK_SPI_SLAVE);
    xTaskNotifyGive(task_handle_wait_spi);

    xTaskCreatePinnedToCore(task_process_buffer, "task_process_buffer", 2048, NULL, 2, &task_handle_process_buffer, CORE_TASK_PROCESS_BUFFER);


    for( int i = 0; i < BUFFER_SIZE; i++) {
        spi_slave_tx_buf[i] = 0;
    }
    delay(1000);
}

void loop() {
    if (newCommand) {
        newCommand = false;
        commandSignal = processCommands(EncoderCommand);
    }

    if (commandSignal == 1) {
        Serial.print("step,x,y,c,s");
        Serial.println();
        commandSignal = 0;
    }
    if (commandSignal == 3 && calibrationMode) {
        stillReading = true;
        startReading = false;
        readEncoder(32);
        // Send signal to serial
        Serial.print(step_count); Serial.print(",");
        Serial.print(encX); Serial.print(",");
        Serial.print(encY); Serial.print(",");
        Serial.print(encC); Serial.print(",");
        Serial.print(encS);
        Serial.println();
        step_count += 1;
        stillReading = false;
        setTXBuffer(DONE_ENC);
        commandSignal = 0;
    }
}

void readEncoder(int samples) {
    encX = 0;
    encY = 0;
    encC = 0;
    encS = 0;

    for (int i =0; i < samples; i++) {
        encX += analogRead(ENC_X);
        encY += analogRead(ENC_Y);
        encC += analogRead(ENC_C);
        encS += analogRead(ENC_S);
    }
    encX /= samples;
    encY /= samples;
    encC /= samples;
    encS /= samples;
}

int processCommands(uint16_t command) {
    switch(command) {
        case STANDBY:
            // STANDBY
            setTXBuffer(STANDBY);
            break;
        case QUERY:
            // QUERY
            break;

        case START_CAL:
            // Set cal mode
            setTXBuffer(START_CAL);
            calibrationMode = true;
            return 1;
        case DONE_STEP:
            // continue reading encoder
            if (!stillReading) {
                startReading = true;
                setTXBuffer(READING_ENC);
                return 3;
            }
            else {
                // not done yet
                Serial.println("WARNIG: Not done reading but got DONE_STEP");
                break;
            }
        case READING_ENC:
            // READY_ENC
            setTXBuffer(STANDBY);
            break;
        case DONE_ENC:
            // DONE_ENC
            setTXBuffer(STANDBY);
            break;
        case DONE_CAL:
            // wrap up calibration
            calibrationMode = false;
            setTXBuffer(DONE_CAL);
            return 2;

        default:
            // What
            break;
    }

    return 0;
}

void setTXBuffer(uint16_t respond) {
    uint8_t byte0 = int(respond/256);
    uint8_t byte1 = respond % 256;
    spi_slave_tx_buf[0] = byte0;
    spi_slave_tx_buf[1] = byte1;
}
